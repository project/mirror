
What is it?
===========
Mirror module is a simple set of helper functions for developers who need to mirror remote image files locally. Your code only needs to know the remote URL of an image file to work with it: Mirror module will cache the image locally and return the new filename. In the future, if you ask for the same image url, it will recognize that it's already been cached and will give you the pre-cached version.

Mirror module also provides helper functions to add the new file to Drupal's standard files table for future reference.  

Why is it useful?
=================
Many Drupal sites use utilities like ImageCache to manipulate image files, cropping and resizing them to fit the site's design. While this works fine with files that are uploaded locally onto the web server, it doesn't work with image files hosted on remote web servers. Flickr photos and Amazon.com product thumbnails are both common examples.

The moment that remote image is cached on the local web server, though, utilities like ImageCache CAN work with it. In addition, if a record for the image is added to Drupal's Files table, the Views module can create filterable, sortable listings of those locally mirrored files as well. All kinds of good stuff happens.


Using mirror.module
===================
Mirror is a developer utility: it provides no "automatic" functionality, no integration with other modules, no nothing. Still, it's pretty easy to use.

If you have the URL of an image file, just call mirror_get($url), and you'll get back the local path of the file in question. Call mirror_save_file($path), and it will return a full Drupal file record (inserting one into the files table if one doesn't already exist).

How does that work in the real world? The following snippet of code checks Flickr for photos tagged with 'blog', then retrieves them and saves them as Drupal nodes using a CCK Image Field:

function mymodule_cron() {
  // We'll use Flickr.module's utility functions here...
  module_load_include('inc', 'flickr');

  $flickr_id = variable_get('flickr_default_userid', '');
  $tags = flickr_tag_request_args(array('blog'));
  $photos = flickr_photos_search($flickr_id, 1, $tags);
  
  foreach ($photos['photo'] as $p) {
    $url = flickr_photo_img($p, 'b');
    $local = mirror_get($url, 'photo');
    // OK, now we've got our locally cached file, saved
    // in a 'photo' directory.
    
    if ($file = mirror_check_file($local)) {
      // The photo has already been retrieved -- don't make a node for it!
      continue;
    }
    else if ($file = mirror_save_file($local)) {
      // We've retrieved the file, saved it to the database... now let's
      // make a node!
      $node = array(
        'type' => 'photo',
        'uid' => 1,
        'status' => 1,
        'promote' => 1,
        'title' => $p['title'],
        'field_photo' => array(0 => $file)
        // The file we get back from mirror_load_file() is exactly
        // what CCK Image fields want when a node is being saved
        // programmatically.
      );

      $node = (object)$node;
      node_validate($node);
      node_save($node);
    }
  }
}

Voila! Automatic Flickr importing -- and anything else you need.